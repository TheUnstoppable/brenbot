FDS Status Reporter Plugin
Version 1.06
Daniel Paul (danpaul88)
danpaul88@yahoo.co.uk


Uploads a .txt file containing current server status data to a website via FTP.

If you want to ask any questions, request new features or report bugs please post on the Renegade Public forums (www.renegadeforums.com) in the Technical Support -> Other area, or email me.


###############
Installation
###############

Extract the plugins folder to your BRenBot folder.


###############
Changelog
###############

1.06
 - Updated to new 1.54 plugin format

1.05
 - Community patch provided by Gen_Blacky. See http://www.renegadeforums.com/index.php?t=msg&th=40432

1.04
 - Support for updating through brLoader
 
1.03
 - Updated to be backwards compatible with 1.50

1.02
 - Compatibility fix for BRenBot 1.51

1.0
 - First Release


###############
Commands
###############

N/A