HTML Output Plugin
Version 1.03
Daniel Paul (danpaul88)
danpaul88@yahoo.co.uk


Creates a webpage with a basic overview of events which happened ingame.

This plugin is based on the htmloutput module as it existed in brenbot 1.41.

If you want to ask any questions, request new features or report bugs please post on the Renegade Public forums (www.renegadeforums.com) in the Technical Support -> Other area, or email me.


###############
Installation
###############

Extract the plugins folder and HTML_Game_Logs folder to your BRenBot folder.


###############
Changelog
###############

1.03
 - Support for updating through brLoader
 
1.02
 - Updated to be backwards compatible with 1.50

1.01
 - Compatibility fix for BRenBot 1.51

1.0
 - First Release


###############
Commands
###############

N/A