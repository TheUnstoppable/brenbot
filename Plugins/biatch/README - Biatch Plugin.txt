BIATCH Plugin
Version 1.031
Daniel Paul (danpaul88)
danpaul88@yahoo.co.uk


This plugin provides an interface to use the console commands in BIATCH, and handles BIATCH
logfile output. For more detailed information about what the commands do refer to the offical
BIATCH documentation at www.black-intel.net

BIATCH is a BlackIntel product.

If you want to ask any questions, request new features or report bugs please post on the Renegade
Public forums (www.renegadeforums.com) in the Technical Support -> Other area, or email me.


###############
Installation
###############

Extract the biatch.pm and biatch.xml files to the BRenBot/plugins folder.


###############
Changelog
###############

1.031
 - Bugfix for the space BI decided to stick in front of [BIATCH]

1.03
 - Updated to work with BIATCH 1.0

1.02
 - Fixed warhead name insertion

1.01
 - Added code to insert warhead names into BIATCH console messages

1.0
 - First Release


###############
Commands
###############

!trust <name/<id>
Trusts the specified player

!untrust <name/<id>
Untrusts the specified player

!antiaimbot <name/<id>
Enables the antiaimbot script for the specified player

!rehashbiatch
Rehashes the BIATCH configuration files.